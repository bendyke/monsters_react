import Appheader from "./components/AppHeader";
import NewMonster from "./components/Monster/NewMonster";
import AppCard from "./components/UI/AppCard";
import { useState } from "react";
import CurrentMonsters from "./components/Monster/CurrentMonsters";
import DUMMY_MONSTERS from "./components/helpers/dummyMonsters";

function App() {
  const [monsters, setMonsters] = useState(DUMMY_MONSTERS);

  function addMonsterHandler(newMonster) {
    setMonsters((prevMonsters) => {
      return [newMonster, ...prevMonsters];
    });
  }

  function deleteMonsterHandler(monsterToDelete) {
    setMonsters((prevMonsters) => {
      let idx = monsters.findIndex((x) => monsterToDelete === x);

      return [...prevMonsters.slice(0, idx), ...prevMonsters.slice(idx + 1)];
    });
  }

  function editMonsterHandler(monsterToUpdate) {
    //find monster from ID
    setMonsters((prevMonsters) => {
      let idx = monsters.findIndex(
        (monster) => monsterToUpdate.id === monster.id
      );

      return [
        ...prevMonsters.slice(0, idx),
        monsterToUpdate,
        ...prevMonsters.slice(idx + 1),
      ];
    });
  }

  return (
    <div className="App">
      <AppCard>
        <Appheader />
        <NewMonster onAddNewMonster={addMonsterHandler} />
        <CurrentMonsters
          monsters={monsters}
          onDeleteMonster={deleteMonsterHandler}
          onEditMonster={editMonsterHandler}
        />
      </AppCard>
    </div>
  );
}

export default App;
