import styled from "styled-components";
import attackSvg from "../../icons/attack.svg";
import defenseSvg from "../../icons/defense.svg";

const MonsterStatsPanel = styled.div`
  display: inline-block;
  margin: var(--margin-normal);
  justify-content: space-between;
  align-items: center;
`;

const MonsterStats = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const AttackSvg = styled.svg`
  background-image: url(${attackSvg});
  height: var(--svg-size_small);
  width: var(--svg-size_small);
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small);
`;
const DefenseSvg = styled.svg`
  background-image: url(${defenseSvg});
  height: var(--svg-size_small);
  width: var(--svg-size_small);
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small);
`;
const MonsterName = styled.h4`
  margin: 0;
`;
const AttackStat = styled.p`
  margin: var(--margin-small__sides);
`;
const DefenseStat = styled.p`
  margin: 0;
`;

function AttributesPanel(props) {
  return (
    <MonsterStatsPanel>
      <MonsterName>{props.name}</MonsterName>
      <MonsterStats>
        <AttackSvg />
        <AttackStat>{props.attack}</AttackStat>
        <DefenseSvg />
        <DefenseStat>{props.defense}</DefenseStat>
      </MonsterStats>
    </MonsterStatsPanel>
  );
}

export default AttributesPanel;
