import styled from "styled-components";

const MonsterImage = styled.svg`
  width: var(--svg-size_large);
  height: var(--svg-size_large);
  background-image: url(${(props) => props.src});
  margin: var(--margin-small__sides);
  border-radius: var(--border-radius__normal);
`;

function ElementImage(props) {
  return (
    <div>
      <MonsterImage src={props.element} />
    </div>
  );
}

export default ElementImage;
