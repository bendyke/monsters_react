import styled from "styled-components";
import media from "styled-media-query";
import { useState } from "react";
import TrashSvg from "../../icons/trash-can.svg";
import EditSvg from "../../icons/logo.svg";
import AttributesPanel from "./AttributesPanel";
import ElementImage from "./ElementImage";
import MonsterEditor from "./MonsterEditor/MonsterEditor";
import Modal from "../../UI/Modal";

const ListElementContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: var(--margin-normal);
  background-color: var(--list-element-color);
  border-width: var(--border-width__normal);
  border-color: var(--list-element-color__border);
  border-style: solid;
  border-radius: var(--border-radius__normal);
  height: 80px;
`;
const MonsterPanel = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const DeleteElementButton = styled.button`
  background-image: url(${TrashSvg});
  border-radius: var(--border-radius__normal);
  ${media.greaterThan("395px")`
    margin: var(--margin-small__sides);
    width: var(--button-size__manage_button);
    height: var(--button-size__manage_button);
  `}
  ${media.lessThan("395px")`
    margin: var(--margin-small__sides);
    width: var(--button-size__manage_button__SMALL);
    height: var(--button-size__manage_button__SMALL);
  `}
`;
const EditMonsterButton = styled.button`
  background-image: url(${EditSvg});
  border-radius: var(--border-radius__normal);
  ${media.greaterThan("395px")`
  margin: var(--margin-small__sides);
    width: var(--button-size__manage_button);
    height: var(--button-size__manage_button);
  `}
  ${media.lessThan("395px")`
    margin: var(--margin-small__sides);
    width: var(--button-size__manage_button__SMALL);
    height: var(--button-size__manage_button__SMALL);
  `}
`;

function MonsterElement(props) {
  const [isEdited, setIsEdited] = useState(false);

  function DeleteClickHandler(event) {
    props.onDeleteMonsterClicked(props.monster);
  }

  function EditClickHandker(event) {
    setIsEdited(true);
  }
  function onCloseWithoutSaveHandler(event) {
    setIsEdited(false);
  }

  if (isEdited === true) {
    return (
      <>
        <Modal onClose={onCloseWithoutSaveHandler}>
          <MonsterEditor
            monster={props.monster}
            setIsEdited={setIsEdited}
            onSaveEdit={props.onEditMonsterSaved}
            onCloseWithoutSave={onCloseWithoutSaveHandler}
          />
        </Modal>
      </>
    );
  }

  return (
    <ListElementContainer>
      <MonsterPanel>
        <ElementImage element={props.monster.element} />
        <AttributesPanel
          name={props.monster.name}
          attack={props.monster.attack}
          defense={props.monster.defense}
        />
      </MonsterPanel>
      <div>
        <EditMonsterButton onClick={EditClickHandker} />
        <DeleteElementButton onClick={DeleteClickHandler} />
      </div>
    </ListElementContainer>
  );
}

export default MonsterElement;
