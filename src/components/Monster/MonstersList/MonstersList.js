import styled from "styled-components";
import MonsterListEntity from "./MonsterListEntity";

const MonstersListFallback = styled.h2`
  justify-content: center;
  align-items: center;
`;
const MonstersUnorderedList = styled.ul`
  padding: 0;
`;

function MonstersList(props) {
  function onDeleteMonsterClicked(monsterToDelete) {
    props.onDeleteMonsterFromList(monsterToDelete);
  }

  function onEditMonsterSaved(monsterToUpdate) {
    props.onEditMonster(monsterToUpdate);
  }

  if (props.monsters.length === 0) {
    return <MonstersListFallback>No Monsters Found</MonstersListFallback>;
  }

  return (
    <MonstersUnorderedList>
      {props.monsters.map((monster) => (
        <MonsterListEntity
          key={monster.id}
          monster={monster}
          onDeleteMonsterClicked={onDeleteMonsterClicked}
          onEditMonsterSaved={onEditMonsterSaved}
        />
      ))}
    </MonstersUnorderedList>
  );
}

export default MonstersList;
