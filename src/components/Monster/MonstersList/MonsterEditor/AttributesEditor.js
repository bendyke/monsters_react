import styled from "styled-components";
import attackSvg from "../../../icons/attack.svg";
import defenseSvg from "../../../icons/defense.svg";

const MonsterStatsPanel = styled.div`
  display: inline-block;
  justify-content: space-between;
  align-items: center;
`;

const MonsterStats = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: center;
`;
const AttackSvg = styled.svg`
  background-image: url(${attackSvg});
  height: var(--svg-size_small);
  width: var(--svg-size_small);
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small);
`;
const DefenseSvg = styled.svg`
  background-image: url(${defenseSvg});
  height: var(--svg-size_small);
  width: var(--svg-size_small);
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small);
`;
const MonsterName = styled.input`
  background-color: ${(props) =>
    props.isValid ? "var(--input-default-bg)" : "var( --input-error-bg)"};
`;
const AttackStat = styled.input`
  margin: var(--margin-small__sides);
  max-width: var(--input-field-size__maxwidth);
  background-color: ${(props) =>
    props.isValid ? "var(--input-default-bg)" : "var( --input-error-bg)"};
`;
const DefenseStat = styled.input`
  max-width: var(--input-field-size__maxwidth);
  background-color: ${(props) =>
    props.isValid ? "var(--input-default-bg)" : "var( --input-error-bg)"};
`;

function AttributesEditor(props) {
  function nameChangeHandler(event) {
    props.nameDispatch({ type: "newInput", val: event.target.value });
  }

  function attackChangeHandler(event) {
    props.attackDispatch({ type: "newInput", val: event.target.value });
  }

  function defenseChangeHandler(event) {
    props.defenseDispatch({ type: "newInput", val: event.target.value });
  }

  return (
    <MonsterStatsPanel>
      <MonsterName
        value={props.currentName.value}
        onChange={nameChangeHandler}
        isValid={props.currentName.isValid}
      />
      <MonsterStats>
        <MonsterStats>
          <AttackSvg />
          <AttackStat
            value={props.currentAttack.value}
            type="number"
            min={0}
            onChange={attackChangeHandler}
            isValid={props.currentAttack.isValid}
          />
        </MonsterStats>
        <MonsterStats>
          <DefenseSvg />
          <DefenseStat
            value={props.currentDefense.value}
            type="number"
            min={0}
            onChange={defenseChangeHandler}
            isValid={props.currentDefense.isValid}
          />
        </MonsterStats>
      </MonsterStats>
    </MonsterStatsPanel>
  );
}

export default AttributesEditor;
