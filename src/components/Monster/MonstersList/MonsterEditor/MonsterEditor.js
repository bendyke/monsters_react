import styled from "styled-components";
import { useState, useReducer } from "react/cjs/react.development";
import ElementPicker from "../../MonsterConstruction/ElementPicker";
import AttributesEditor from "./AttributesEditor";

const EditorContainer = styled.div`
  max-width: 600px;
  margin: auto;
  background: var(--editor-background);
  border-width: var(--border-width__normal);
  border-color: var(--list-element-color__border);
  border-style: solid;
  border-radius: var(--border-radius__normal);
`;
const SaveEditContainer = styled.div`
  justify-content: center;
  align-content: center;
`;
const EditMonsterButton = styled.button`
  height: var(--button-size__manage_button);
  margin-bottom: var(--margin-normal);
  margin-top: var(--margin-normal);
  background: black;
  color: white;
  font-family: var(--font-normal);
  font-size: var(--font-size__large);
  border-radius: var(--border-radius__normal);
`;
const CloseModalDiv = styled.div`
  height: var(--svg-size_normal);
  width: 100%;
`;
const CloseModalButton = styled.button`
  float: right;
  width: var(--svg-size_normal);
  height: var(--svg-size_normal);
  font-size: var(--font-size__large);
  color: white;
  background-color: black;
  border-radius: var(--border-radius__normal);
`;

function inputReducer(state, action) {
  switch (action.type) {
    case "newInput":
      return { value: action.val, isValid: state.isValid };
    case "invalid":
      return { value: state.value, isValid: false };
    case "valid":
      return { value: state.value, isValid: true };
    default:
      throw new Error();
  }
}

function MonsterEditor(props) {
  const [activeElement, setActiveElement] = useState(props.monster.element);

  const [currentName, nameDispatch] = useReducer(inputReducer, {
    value: props.monster.name,
    isValid: true,
  });
  const [currentAttack, attackDispatch] = useReducer(inputReducer, {
    value: props.monster.attack,
    isValid: true,
  });
  const [currentDefense, defenseDispatch] = useReducer(inputReducer, {
    value: props.monster.defense,
    isValid: true,
  });

  function submitHandler(event) {
    event.preventDefault();

    if (
      currentName.value !== "" &&
      currentAttack.value !== "" &&
      currentDefense.value !== ""
    ) {
      nameDispatch({ type: "valid" });
      attackDispatch({ type: "valid" });
      defenseDispatch({ type: "valid" });
      const editedMonster = {
        name: currentName.value,
        //match ID to Find() with later in the database
        id: props.monster.id,
        element: activeElement,
        attack: +currentAttack.value,
        defense: +currentDefense.value,
      };

      props.onSaveEdit(editedMonster);

      props.setIsEdited(false);

      setActiveElement("");
      nameDispatch({ type: "newInput", val: "" });
      attackDispatch({ type: "newInput", val: "" });
      defenseDispatch({ type: "newInput", val: "" });
    } else {
      if (currentName.value === "") {
        nameDispatch({ type: "invalid" });
      } else {
        nameDispatch({ type: "valid" });
      }
      if (currentAttack.value === "") {
        attackDispatch({ type: "invalid" });
      } else {
        attackDispatch({ type: "valid" });
      }
      if (currentDefense.value === "") {
        defenseDispatch({ type: "invalid" });
      } else {
        defenseDispatch({ type: "valid" });
      }
    }
  }

  return (
    <>
      <EditorContainer>
        <CloseModalDiv>
          <CloseModalButton onClick={props.onCloseWithoutSave}>
            X
          </CloseModalButton>
        </CloseModalDiv>
        <ElementPicker
          activeElement={activeElement}
          setActiveElement={setActiveElement}
        />
        <AttributesEditor
          monster={props.monster}
          currentName={currentName}
          currentAttack={currentAttack}
          currentDefense={currentDefense}
          nameDispatch={nameDispatch}
          attackDispatch={attackDispatch}
          defenseDispatch={defenseDispatch}
        />
        <SaveEditContainer>
          <EditMonsterButton onClick={submitHandler}>SAVE</EditMonsterButton>
        </SaveEditContainer>
      </EditorContainer>
    </>
  );
}

export default MonsterEditor;
