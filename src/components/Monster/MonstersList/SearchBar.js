import styled from "styled-components";

const SearchBar = styled.input`
  display: flex;
  margin: var(--margin-large__sides);
  border-radius: var(--border-radius__round);
  height: var(--svg-size_small);
`;

function SearchMonsters(props) {
  function filterInputChangeHandler(event) {
    props.onFilterInputChanged(event.target.value);
  }

  return (
    <div>
      <SearchBar
        value={props.currentFilterInput}
        placeholder={" Search"}
        onChange={filterInputChangeHandler}
      />
    </div>
  );
}

export default SearchMonsters;
