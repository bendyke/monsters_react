import attackSvg from "../../icons/attack.svg";
import defenseSvg from "../../icons/defense.svg";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: center;
  margin: var(--margin-normal__spacer);
`;
const AttackImg = styled.svg`
  background-image: url(${attackSvg});
  height: var(--svg-size_normal);
  width: var(--svg-size_normal);
  margin: var(--margin-small__sides);
  border-radius: var(--border-radius__normal);
`;
const DefenseImg = styled.svg`
  background-image: url(${defenseSvg});
  height: var(--svg-size_normal);
  width: var(--svg-size_normal);
  margin: var(--margin-small__sides);
  border-radius: var(--border-radius__normal);
`;
const StatDiv = styled.div`
  display: flex;
`;
const Input = styled.input`
  border-radius: var(--border-radius__normal);
  background-color: ${(props) =>
    props.isValid ? "var(--input-default-bg)" : "var( --input-error-bg)"};
`;

function StatPicker(props) {
  function onAttackChanged(event) {
    props.attackDispatch({ type: "newInput", val: event.target.value });
  }

  function onDefenseChanged(event) {
    props.defenseDispatch({ type: "newInput", val: event.target.value });
  }

  return (
    <Wrapper>
      <StatDiv>
        <AttackImg />
        <Input
          type="number"
          value={props.currentAttack.value}
          min={0} //nem tudom kell e ez, de hasznaltam
          onChange={onAttackChanged}
          isValid={props.currentAttack.isValid}
        />
      </StatDiv>
      <StatDiv>
        <DefenseImg />
        <Input
          type="number"
          value={props.currentDefense.value}
          min={0}
          onChange={onDefenseChanged}
          isValid={props.currentDefense.isValid}
        />
      </StatDiv>
    </Wrapper>
  );
}

export default StatPicker;
