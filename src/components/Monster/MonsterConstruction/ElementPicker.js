import styled from "styled-components";
import { useState, useEffect } from "react";
import ArrowLeft from "../../icons/arrow-left.svg";
import ArrowRight from "../../icons/arrow-right.svg";
import airElement from "../../icons/air.svg";
import earthElement from "../../icons/earth.svg";
import fireElement from "../../icons/fire.svg";
import waterElement from "../../icons/water.svg";

const ElementPickerStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: var(--margin-normal__spacer);
`;
const LeftArrow = styled.button`
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small__sides);
  height: var(--svg-size_normal);
  width: var(--svg-size_normal);
  background-image: url(${ArrowLeft});
`;
const RightArrow = styled.button`
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small__sides);
  height: var(--svg-size_normal);
  width: var(--svg-size_normal);
  background-image: url(${ArrowRight});
`;
const elements = [airElement, earthElement, fireElement, waterElement];

const PickedElement = styled.svg`
  border-radius: var(--border-radius__normal);
  margin: var(--margin-small__sides);
  max-height: var(--svg-size_large);
  max-width: var(--svg-size_large);
  background-image: url(${(props) => props.src});
`;

function ElementPicker(props) {
  const [elementsIndex, setElementsIndex] = useState(0);

  //reset function
  useEffect(() => {
    if (props.activeElement === "") {
      setElementsIndex(0);
      props.setActiveElement(elements[0]);
    }
  }, [props]);

  function backClickHandler(event) {
    if (elements[elementsIndex - 1]) {
      props.setActiveElement(elements[elementsIndex - 1]);
      setElementsIndex(elementsIndex - 1);
    }
  }

  function forwardClickhandler(event) {
    if (elements[elementsIndex + 1]) {
      props.setActiveElement(elements[elementsIndex + 1]);
      setElementsIndex(elementsIndex + 1);
    }
  }

  return (
    <ElementPickerStyled>
      <LeftArrow type="button" onClick={backClickHandler} />
      <PickedElement src={props.activeElement} />
      <RightArrow type="button" onClick={forwardClickhandler} />
    </ElementPickerStyled>
  );
}

export default ElementPicker;
