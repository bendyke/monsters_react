import ElementPicker from "./ElementPicker";
import { Fragment, useState, useReducer } from "react";
import styled from "styled-components";
import StatPicker from "./StatPicker";

const NameInput = styled.input`
  display: flex;
  margin: auto;
  height: var(--input-field-size__normal);
  border-radius: var(--border-radius__normal);
  background-color: ${(props) =>
    props.isValid ? "var(--input-default-bg)" : "var( --input-error-bg)"};
`;
const SubmitButton = styled.button`
  display: flex;
  margin: auto;
  justify-content: center;
  align-items: center;
  width: 8rem;
  height: var(--button-size__manage_button);
  margin-bottom: var(--margin-normal);
  margin-top: var(--margin-normal);
  background: black;
  color: white;
  font-family: var(--font-normal);
  font-size: var(--font-size__large);
  border-radius: var(--border-radius__normal);
`;

const ErrorMessage = styled.p`
  display: flex;
  margin: auto;
  width: fit-content;
  background-color: var(--error-message-color);
  border-radius: var(--border-radius__normal);
  padding: var(--padding-normal);
`;

function inputReducer(state, action) {
  switch (action.type) {
    case "newInput":
      return { value: action.val, isValid: state.isValid };
    case "invalid":
      return { value: state.value, isValid: false };
    case "valid":
      return { value: state.value, isValid: true };
    default:
      throw new Error();
  }
}

function MonsterForm(props) {
  const [activeElement, setActiveElement] = useState("");
  const [showError, setShowError] = useState(false);
  const [currentName, nameDispatch] = useReducer(inputReducer, {
    value: "",
    isValid: true,
  });
  const [currentAttack, attackDispatch] = useReducer(inputReducer, {
    value: "",
    isValid: true,
  });
  const [currentDefense, defenseDispatch] = useReducer(inputReducer, {
    value: "",
    isValid: true,
  });

  function nameChangeHandler(event) {
    nameDispatch({ type: "newInput", val: event.target.value });
  }

  function submitHandler(event) {
    event.preventDefault();

    if (
      currentName.value !== "" &&
      currentAttack.value !== "" &&
      currentDefense.value !== ""
    ) {
      setShowError(false);
      nameDispatch({ type: "valid" });
      attackDispatch({ type: "valid" });
      defenseDispatch({ type: "valid" });
      const newMonsterData = {
        name: currentName.value,
        element: activeElement,
        attack: +currentAttack.value,
        defense: +currentDefense.value,
      };

      props.onSaveNewMonster(newMonsterData);

      setActiveElement("");
      nameDispatch({ type: "newInput", val: "" });
      attackDispatch({ type: "newInput", val: "" });
      defenseDispatch({ type: "newInput", val: "" });
    } else {
      setShowError(true);
      if (currentName.value === "") {
        nameDispatch({ type: "invalid" });
      } else {
        nameDispatch({ type: "valid" });
      }
      if (currentAttack.value === "") {
        attackDispatch({ type: "invalid" });
      } else {
        attackDispatch({ type: "valid" });
      }
      if (currentDefense.value === "") {
        defenseDispatch({ type: "invalid" });
      } else {
        defenseDispatch({ type: "valid" });
      }
    }
  }

  return (
    <Fragment>
      <form onSubmit={submitHandler}>
        <ElementPicker
          activeElement={activeElement}
          setActiveElement={setActiveElement}
        />
        <NameInput
          value={currentName.value}
          placeholder={"Name"}
          onChange={nameChangeHandler}
          isValid={currentName.isValid}
        />
        <StatPicker
          attackDispatch={attackDispatch}
          defenseDispatch={defenseDispatch}
          currentAttack={currentAttack}
          currentDefense={currentDefense}
        />
        <SubmitButton type="submit">Add</SubmitButton>
        {showError && (
          <div>
            <ErrorMessage>Please Fill out all fields!</ErrorMessage>
          </div>
        )}
      </form>
    </Fragment>
  );
}

export default MonsterForm;
