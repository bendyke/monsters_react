import styled from "styled-components";
import Card from "../UI/Card";
import MonsterForm from "./MonsterConstruction/MonsterForm";

const NewMonsterHeader = styled.h3`
  text-align: left;
  margin: var(--margin-normal);
`;

function NewMonster(props) {
  function saveNewMonsterHandler(savedMonsterData) {
    const monsterData = {
      ...savedMonsterData,
      id: "A" + Math.random().toString(),
    };
    props.onAddNewMonster(monsterData);
  }

  return (
    <Card>
      <NewMonsterHeader>Create Monster</NewMonsterHeader>
      <MonsterForm onSaveNewMonster={saveNewMonsterHandler} />
    </Card>
  );
}

export default NewMonster;
