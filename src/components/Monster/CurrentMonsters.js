import styled from "styled-components";
import { useState } from "react";
import Card from "../UI/Card";
import MonstersList from "./MonstersList/MonstersList";
import SearchBar from "./MonstersList/SearchBar";

const MonstersHeader = styled.h3`
  text-align: left;
  margin: var(--margin-normal);
`;

function CurrentMonsters(props) {
  const [filterInput, setFilterInput] = useState("");

  const filteredMonsters = props.monsters.filter((monster) => {
    return monster.name.toLowerCase().includes(filterInput.toLowerCase());
  });

  function filterInputChangeHandler(newFilterInput) {
    setFilterInput(newFilterInput);
  }

  return (
    <Card>
      <MonstersHeader>Monsters</MonstersHeader>
      <SearchBar onFilterInputChanged={filterInputChangeHandler} />
      <MonstersList
        monsters={filteredMonsters}
        onDeleteMonsterFromList={props.onDeleteMonster}
        onEditMonster={props.onEditMonster}
      />
    </Card>
  );
}

export default CurrentMonsters;
