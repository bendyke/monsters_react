import { ReactComponent as Logo } from "./icons/logo.svg";
import styled from "styled-components";

const HeaderImage = styled(Logo)`
  height: var(--logo-size);
  width: var(--logo-size);
`;
const HeaderText = styled.h1`
  margin: 0;
`;
function Appheader() {
  return (
    <div>
      <HeaderImage />
      <HeaderText>Monsters</HeaderText>
    </div>
  );
}
export default Appheader;
