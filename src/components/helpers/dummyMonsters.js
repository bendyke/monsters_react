const DUMMY_MONSTERS = [
  {
    name: "Bélu",
    id: "A01",
    element: "/static/media/fire.321d7353d8280810a37ab0c3b8fcbcc0.svg",
    attack: 5,
    defense: 3,
  },
  {
    name: "BigMan",
    id: "A02",
    element: "/static/media/air.ff5ee97a02337fd4dc73ae1ec7d05d10.svg",
    attack: 2,
    defense: 3,
  },
  {
    name: "Murci",
    id: "A03",
    element: "/static/media/water.e7e782fe1e90aff00ca33c38f7baea94.svg",
    attack: 2,
    defense: 10,
  },
];

export default DUMMY_MONSTERS;
