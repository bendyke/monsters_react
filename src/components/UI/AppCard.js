import styled from "styled-components";

const AppCardStyled = styled.div`
  border-radius: var(--border-radius__round);
  background-color: var(--app-bg-color);
  margin: auto;
  padding-bottom: var(--padding-large);
  max-width: var(--max-app-width);
  text-align: center;
  justify-content: center;
`;

function AppCard(props) {
  return <AppCardStyled>{props.children}</AppCardStyled>;
}

export default AppCard;
