import styled from "styled-components";

const CardStyled = styled.div`
  border-radius: var(--border-radius__round);
  background-color: var(--content-bg-color);
  margin: auto;
  padding-bottom: var(--padding-large);
  max-width: var(--max-card-width);
`;

function Card(props) {
  return <CardStyled>{props.children}</CardStyled>;
}

export default Card;
